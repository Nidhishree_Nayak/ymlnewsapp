//
//  SignUpModel.swift
//  newsApplication
//
//  Created by Nidhishree on 05/09/19.
//  Copyright © 2019 YML. All rights reserved.
//

import Foundation

struct SignupModel {
    var email: String
    var password: String
    var confirmPassword: String
}
