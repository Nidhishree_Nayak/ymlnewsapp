//
//  Extension+UIColor.swift
//  newsApplication
//
//  Created by Nidhishree on 11/09/19.
//  Copyright © 2019 YML. All rights reserved.
//

import Foundation
import  UIKit

// MARK: - UIColor 
extension UIColor {
    class func getOrange() -> UIColor {
        return UIColor( displayP3Red: 250 / 256, green: 132 / 256, blue: 79 / 256, alpha: 1.0 )
    }
}
