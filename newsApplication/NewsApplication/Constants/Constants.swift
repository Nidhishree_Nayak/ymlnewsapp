//
//  Constants.swift
//  newsApplication
//
//  Created by Nidhishree on 11/09/19.
//  Copyright © 2019 YML. All rights reserved.
//

import Foundation

enum LoginConstants {
    static let signIn = "Sign In"
    static let  signUp = "Sign Up"
}

enum LoginTitleConstants {
    static let signIn = "SIGN IN"
    static let signUp = "SIGN UP"
}

enum UserInputsConstants {
    static let email = "Email"
    static let password = "Password"
    static let confirmPassword = "Confirm Password"
    static let userInput = ""
}
enum PlaceholderConstants {
    static let email = "Enter your email"
    static let password = "Enter your password"
    static let confirmPassword = "Confirm your password"
}
enum NavigationConstants {
    static let signUpVc = "SignUpVC"
    static let newsFeedVc = "NewsFeedVC"
}
enum StoryboardConstants {
    static let newsFeed = "NewsFeed"
}
enum AlertMessageConstants {
    static let alertTitle = "Error"
    static let alertMessage = "This Email Id or Password is invalid"
    static let alertActionTitle = "OK"
}
